(function() {
    let presets = {
        comfyOutdoors: {
            name: 'Comfy Outdoors',
            icon: 'landscape',
            description: 'Enjoy some soothing camping sounds',
            categories: {
                night: 0.3,
                wind: 0.25,
                campfire: 0.7
            }
        },
        two: {
            name: 'Urban City',
            icon: 'city',
            description: 'Listen to city life',
            categories: {
                night: 0.5,
                whiteNoise: 0.2
            }
        },
        three: {
            name: 'Place Two',
            icon: 'landscape',
            description: 'something something default one nice',
            categories: {
                night: 0.5
            }
        },
        four: {
            name: 'Place Three',
            icon: 'landscape',
            description: 'something something default one nice',
            categories: {
                night: 0.5
            }
        }
    };

    let categories = {
        campfire: {
            name: 'Campfire',
            base: './Campfire/',
            files: [
                '170247__matucha__campfire-01.mp3',
                '40699__spandau__campfire.mp3',
                '260713__daenerys__fire-crackling.mp3',
                '204348__sauron974__crackling-fire.mp3',
                '65795__juskiddink__bonfire.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'fire'
        },
        night: {
            name: 'Night Ambience',
            base: './Night/',
            files: [
                '44473__bullockjs__summer-night.mp3',
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'moon'
        },
        rain: {
            name: 'Rain',
            base: './Rain/',
            files: [
                '44471__bullockjs__heavy-rain.mp3',
                '344430__babuababua__light-rain.mp3',
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            section: 'water',
            icon: 'rain'
        },
        rainOnRoof: {
            name: 'Rain on Roof',
            base: './RainOnRoof/',
            files: [
                '184627__drzoom__heavy-rain-in-the-night-on-roof-and-window.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            section: 'miscellaneous',
            icon: 'rain'
        },
        stream: {
            name: 'Water Streams',
            base: './Stream/',
            files: [
                '44472__bullockjs__mountain-stream.mp3',
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            section: 'water',
            icon: 'drop'
        },
        thunder: {
            name: 'Thunder',
            base: './Thunder/',
            files: [
                '2523__rhumphries__rbh-thunder-storm.mp3',
                '243629__lebaston100__heavy-rain.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'power'
        },
        waterfall: {
            name: 'Waterfall',
            base: './Waterfall/',
            files: [
                '44301__bullockjs__waterfall.mp3',
                '44474__bullockjs__waterfall2.mp3',
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            section: 'water',
            icon: 'drop'
        },
        wind: {
            name: 'Wind',
            base: './Wind/',
            files: [
                '44734__bullockjs__autumn-wind.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'wind'
        },
        whiteNoise: {
            name: 'White Noise',
            base: './WhiteNoise/',
            files: [
                '132275__assett1__74-minutes-of-relaxing-soft-noise.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'fan',
            section: 'miscellaneous'
        },
        pinkNoise: {
            name: 'Pink Noise',
            base: './PinkNoise/',
            files: [
                'pinknoise.mp3'
            ],
            howl: null,
            volume: 0.5,
            isPlaying: false,
            icon: 'tape2',
            section: 'miscellaneous'
        }
    };

    let appContainer = document.getElementById('sounds');

    let ui = {
        play: function(category, file) {
            appContainer.dispatchEvent(new CustomEvent('asg:play', { detail: { category: category, file: file }}));
        },

        pause: function(category) {

        },

        changeVolume: function(category) {

        },

        update: function(category) {
            appContainer.dispatchEvent(new CustomEvent('asg:update', { detail: { category: category }}));
        }
    };

    function autoplay(i, category) {
        category.howl = new Howl({
            src: [ category.base + category.files[i] ],
            html5: true,
            preload: true,
            onload: function() {
                category.howl.fade(0, category.volume, 5000);
                let duration = Math.round(category.howl.duration() * 1000);
                setTimeout(function() {
                    category.howl.fade(category.volume, 0, 5000);
                }, duration - 5000);
            },
            onend: function() {
                if ((i + 1) == category.files.length) {
                    autoplay(0, category);
                }
                else {
                    autoplay(i + 1, category);
                }
            },
            volume: 0//category.volume
        });
        category.howl.play();

        ui.play(category, category.files[i]);
    }

    let soundContainer = document.getElementById('sounds');

    //Set up presets
    let presetContainer = document.createElement('section');
    presetContainer.classList.add('preset-section');
    let presetNames = Object.keys(presets);
    presetNames.forEach(n => {
        let preset = presets[n];
        let presetCategories = Object.keys(preset.categories);

        let categoryIconContainer = document.createElement('span');
        categoryIconContainer.classList.add('categories');
        presetCategories.forEach(cat => {
            let categoryData = preset.categories[cat];
            let categoryIcon = categories[cat].icon;
            let opacity = 0.5 + (categoryData / 2);
            categoryIconContainer.innerHTML += `
                <span class="icon icon-${categoryIcon}" style="opacity: ${opacity};"></span>
            `;
        });

        presetContainer.innerHTML += `
            <a class="preset" data-preset="${n}" data-enabled="false">
                <span class="preset-title">
                    <span class="icon icon-${preset.icon}"></span>
                    ${preset.name}
                </span>
                <span class="description">${preset.description}</span>
                ${categoryIconContainer.outerHTML}
            </a>
        `
    });

    soundContainer.appendChild(presetContainer);

    //Set up sections
    let sectionNames = [ 'Recommended', 'Water', 'Miscellaneous' ];
    sectionNames.forEach(s => {
        let sectionSlug = s.replace(' ', '-').toLowerCase();
        let opened = sectionSlug === 'recommended';
        let initialIconClass = (opened) ? 'chevron-up' : 'chevron-down';
        soundContainer.innerHTML += `
            <div class="sound-section ui-toggle" data-section="${sectionSlug}" data-toggle=".open" data-opened="${opened}">
                <a class="section-title">
                    ${s}
                    <span class="icon icon-${initialIconClass}"></span>
                </a>
                <div class="open"><div class="grid-wrapper"></div></div>
            </div>
        `;
    })

    let categoryNames = Object.keys(categories);

    categoryNames.forEach(n => {
        let cat = categories[n];
        let section = categories[n].section || 'recommended';
        document.querySelector(`.sound-section[data-section="${section}"] .open .grid-wrapper`).innerHTML +=  `
            <div class="sound-container" data-category="${n}">
                <div class="wrapper">
                    <button role="button" class="sound-button" data-category="${n}">
                        <span class="icon-${categories[n].icon}"></span>
                        ${categories[n].name}
                    </button>
                    <input type="range" min="0" max="100" class="slider sound-volume" data-category="${n}" value="${categories[n].volume*100}"/>
                </div>
            </div>
        `;
    });

    document.querySelectorAll('.preset-section .preset').forEach(item => {
        item.addEventListener('click', function(event) {
            let preset = presets[this.dataset.preset];
            let disabled = (this.dataset.enabled == 'false');

            this.dataset.enabled = disabled ? 'true' : 'false';
            let categoryNames = Object.keys(preset.categories);

            categoryNames.forEach(n => {
                let cat = categories[n];
                document.querySelector(`button.sound-button[data-category="${n}"]`).click();

                let slider = document.querySelector(`input.sound-volume[data-category="${n}"]`);
                slider.value = 100 * preset.categories[n];
                slider.dispatchEvent(new Event('change'));
            })
        });
    })

    document.querySelectorAll('div.sound-container button.sound-button').forEach(item => {
        item.addEventListener('click', function(event) {
            this.parentNode.parentNode.classList.toggle('playing');
            let cat = categories[this.dataset.category];
            if (!cat.isPlaying) {
                cat.isPlaying = true;
                if (null !== cat.howl) { //Starting existing session
                    cat.howl.play();
                }
                else { //Starting new session
                    let idx = parseInt(Math.random() * cat.files.length);
                    autoplay(idx, cat);
                }
            }
            else {
                cat.howl.stop();
                cat.isPlaying = false;
            }


            ui.update(cat);
        });
    });

    document.querySelectorAll('div.sound-container input.sound-volume').forEach(item => {
        item.addEventListener('change', function() {
            let cat = categories[this.dataset.category];
            if (cat.isPlaying) {
                cat.volume = parseFloat(this.value / 100);
                ui.changeVolume(cat);
                cat.howl.volume(cat.volume);
            }
            ui.update(cat);
        })
    });

    // Misc: set up toggles
    document.querySelectorAll('div.ui-toggle').forEach(item => {
        let toggle = item.querySelector(item.dataset.toggle);
        toggle.style.display = (item.dataset.opened == 'false') ? 'none' : 'block';
        item.querySelector('a').addEventListener('click', function() {
            let icon = this.querySelector('span.icon');
            console.log(this);
            console.log(icon);
            let notOpened = (item.dataset.opened == 'false');
            console.log(notOpened);
            if (notOpened) {
                icon.classList.remove('icon-chevron-down');
                icon.classList.add('icon-chevron-up');
            }
            else {
                icon.classList.remove('icon-chevron-up');
                icon.classList.add('icon-chevron-down');
            }
            item.dataset.opened = notOpened ? 'true' : 'false';
            toggle.style.display = notOpened ? 'block' : 'none';
        })
    })

    //check hash url
    if (window.location.hash) {
        setTimeout(() => {
            let hash = atob(window.location.hash.replace('#', ''));
            if (hash) {
                let hashCategories = hash.split(';');
                hashCategories.forEach(i => {
                    let parts = i.split(':');
                    let cat = categories[parts[0]];
                    if (cat) {
                        document.querySelector(`button.sound-button[data-category="${parts[0]}"]`).click();

                        let slider = document.querySelector(`input.sound-volume[data-category="${parts[0]}"]`);
                        slider.value = 100 * parseFloat(parts[1]);
                        slider.dispatchEvent(new Event('change'));
                    }
                })
            }
        }, 500);

    }

    appContainer.addEventListener('asg:update', function() {
        let hashString = '';
        categoryNames.forEach(n => {

            let cat = categories[n];
            if (cat.isPlaying) {
                hashString += `${n}:${cat.volume};`;
            }
            hashString = hashString.slice(0, -1);

        });
        window.location.hash = btoa(hashString);
    })
})();