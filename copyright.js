(function() {
    let copyright = {
        '184627__drzoom__heavy-rain-in-the-night-on-roof-and-window.mp3': {
            name: 'Heavy Rain in the Night on Roof and Window',
            author: 'DrZoom',
            link: 'https://freesound.org/people/DrZoom/sounds/184627/',
            license: 'CC BY 3.0'
        },
        '65795__juskiddink__bonfire.mp3': {
            name: 'Bonfire',
            author: 'jsutkiddink',
            link: 'https://freesound.org/people/juskiddink/sounds/65795/',
            license: 'CC BY 3.0'
        },
        '132275__assett1__74-minutes-of-relaxing-soft-noise.mp3': {
            name: 'Relaxing Soft Noise',
            author: 'assett1',
            link: 'https://freesound.org/people/assett1/sounds/132275/',
            license: 'CC 0'
        },
        '204348__sauron974__crackling-fire.mp3': {
            name: 'Crackling Fire',
            author: 'sauron974',
            link: 'https://freesound.org/people/Sauron974/sounds/204348/',
            license: 'CC 0'
        },
        '260713__daenerys__fire-crackling.mp3': {
            name: 'Fire Crackling',
            author: 'daenerys',
            link: 'https://freesound.org/people/daenerys/sounds/260713/',
            license: 'CC BY 3.0'
        },
        '170247__matucha__campfire-01.mp3': {
            name: 'Campfire',
            author: 'matucha',
            link: 'https://freesound.org/people/matucha/sounds/170247/',
            license: 'CC BY 3.0'
        },
        '40699__spandau__campfire.mp3': {
            name: 'Campfire',
            author: 'Spandau',
            link: 'https://freesound.org/people/Spandau/sounds/40699/',
            license: 'CC 0'
        },
        // freesound, bullockjs
        '44473__bullockjs__summer-night.mp3': {
            name: 'Summer Night',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44473/',
            license: 'CC BY 3.0'
        },
        '44471__bullockjs__heavy-rain.mp3': {
            name: 'Heavy Rain',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44471/',
            license: 'CC BY 3.0'
        },
        '44472__bullockjs__mountain-stream.mp3': {
            name: 'Mountain Stream',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44472/',
            license: 'CC BY 3.0'
        },
        '44734__bullockjs__autumn-wind.mp3': {
            name: 'Autumn Wind',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44734/',
            license: 'CC BY 3.0'
        },
        '44301__bullockjs__waterfall.mp3': {
            name: 'Waterfall',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44301/',
            license: 'CC BY 3.0'
        },
        '44474__bullockjs__waterfall2.mp3': {
            name: 'Waterfall2',
            author: 'bullockjs',
            link: 'https://freesound.org/people/bullockjs/sounds/44474/',
            license: 'CC BY 3.0'
        },
        // freesound, babuababua
        '344430__babuababua__light-rain.mp3': {
            name: 'Light Rain',
            author: 'babuababua',
            link: 'https://freesound.org/people/babuababua/sounds/344430/',
            license: 'CC BY 3.0'
        },
        // freesound, lebaston100
        '243629__lebaston100__heavy-rain.mp3': {
            name: 'Heavy Rain',
            author: 'lebaston100',
            link: 'https://freesound.org/people/lebaston100/sounds/243629/',
            license: 'CC BY 3.0'
        },
        // freesound, RHumphries
        '2523__rhumphries__rbh-thunder-storm.mp3': {
            name: 'rbh thunder storm',
            author: 'RHumphries',
            link: 'https://freesound.org/people/RHumphries/sounds/2523/',
            license: 'CC BY 3.0'
        }
    };
    document.getElementById('sounds').addEventListener('asg:play', function(e) {
        console.log(e);
        if (copyright.hasOwnProperty(e.detail.file)) {
            let data = copyright[e.detail.file];
            let template = `
                <span class="icon icon-${e.detail.category.icon}"></span>
                Now playing <a href="${data.link}">${data.name} by ${data.author}</a>
            `;

            let node = document.createElement('div');
            node.innerHTML = template;
            node.classList.add('toast');

            document.getElementById('toast').appendChild(node);

            setTimeout(function() {
                node.remove();
            }, 7500);
        }
    });
})();